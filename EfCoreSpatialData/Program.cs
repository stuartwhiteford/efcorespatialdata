﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeoAPI.Geometries;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;

namespace EfCoreSpatialData
{
    class Program
    {
        private static SpatialDataContext dataContext;

        static void Main(string[] args)
        {
            SetDataContext();
            Test01();
            Console.WriteLine("Press ENTER to continue.");
            Console.ReadLine();
        }

        private static void Test01()
        {

            Point addressLocation = new Point(-4, 55)
            {
                SRID = 4326
            };

            List<Coordinate> searchCoordinates = new List<Coordinate>
            {
                new Coordinate(-5, 56),
                new Coordinate(-3, 56),
                new Coordinate(-3, 54),
                new Coordinate(-5, 54),
                new Coordinate(-5, 56)
            };

            Coordinate[] searchCoordinatesArray = searchCoordinates.ToArray();
            LinearRing searchLinearRing = new LinearRing(searchCoordinatesArray);
            Polygon searchPolygon = new Polygon(searchLinearRing)
            {
                SRID = 4326
            };

            IGeometry searchGeometry = searchPolygon.Normalized().Reverse();
            searchGeometry.SRID = 4326;

            bool locationIsInSearchPolygon = searchGeometry.Contains(addressLocation);
            Console.WriteLine(locationIsInSearchPolygon); // This is true.

            //// List<Address> addresses = dataContext.Addresses.Where(x => searchGeometry.Contains(x.Location)).ToList(); // This throws System.Data.SqlClient.SqlException: 'Cannot call methods on varbinary.'
            List<Address> addresses = dataContext.Addresses.Where(x => x.Location.Within(searchGeometry)).ToList(); // This throws System.Data.SqlClient.SqlException: 'Cannot call methods on varbinary.'
            
            foreach (var address in addresses)
            {
                Console.WriteLine(address.Line1);
            }
        }

        private static void SetDataContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<SpatialDataContext>();
            var connectionString = "Server=.;Database=Spatial;Trusted_Connection=True;";
            optionsBuilder.UseSqlServer(connectionString, options => options.UseNetTopologySuite());
            dataContext = new SpatialDataContext(optionsBuilder.Options);
        }
    }
}
