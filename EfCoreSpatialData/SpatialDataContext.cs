﻿using Microsoft.EntityFrameworkCore;

namespace EfCoreSpatialData
{
    public class SpatialDataContext : DbContext
    {
        public SpatialDataContext(DbContextOptions<SpatialDataContext> options)
            : base(options)
        { }

        public DbSet<Address> Addresses { get; set; }
    }
}
